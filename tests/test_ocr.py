# pylint: disable=C0114,C0115,C0116,C0303

import unittest

from ocr import scan, Unimplemented


class TestOCR(unittest.TestCase):
    def test_ocr_scan_zero(self):
        input_grid = """ _ 
| |
|_|
   """

        digits = scan(input_grid)

        self.assertEqual("0", digits)

    def test_ocr_scan_one(self):
        input_grid = """   
  |
  |
   """

        digits = scan(input_grid)

        self.assertEqual("1", digits)

    def test_ocr_scan_all_digits(self):
        input_grid = """    _  _     _  _  _  _  _  _ 
  | _| _||_||_ |_   ||_||_|| |
  ||_  _|  | _||_|  ||_| _||_|
                              """

        digits = scan(input_grid)

        self.assertEqual("1234567890", digits)

    def test_ocr_scan_invalid_grid(self):
        input_grid = """______
|
______
      """

        self.assertRaises(Unimplemented, scan(input_grid))  # TODO: check the error type

    def test_ocr_scan_invalid_characters(self):
        input_grid = """      
 _ _ _
  | | 
      """

        self.assertRaises(Unimplemented, scan(input_grid))  # TODO: check the error type


if __name__ == '__main__':
    unittest.main()
