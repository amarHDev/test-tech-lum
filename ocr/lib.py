"""OCR implementation"""
from .errors import Unimplemented


def scan(_digits_grid: str) -> str:
    """Returns recognized digits as string"""

    # TODO:
    #  Implement digit recognition here.
    #  Feel free to split and organize your code as you see fit!
    raise Unimplemented
