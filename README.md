OCR
===

# Instructions

Le but de cet exercice est d'implémenter une fonction `scan` permettant de reconnaître des chiffres "digitaux".

## Données

Chaque chiffre est écrit :
 - à partir des caractères suivants :
   - `|` _pipe_
   - `_` _underscore_
   - ` ` _space_
 - sur une grille de 3 colonnes sur 4 lignes
 - la quatrième et dernière ligne est toujours vide (trois caractères ` ` uniquement)

### Exemples :

Le "0" est représenté par :
```
 _ 
| |
|_|
   
```

Le "1" est représenté par :
```
   
  |
  |
   
```

"45" serait donc représenté par :
```
    _ 
|_||_ 
  | _|
      
```

## Erreurs

En cas de caractère invalide ou illisible, ou autre événement inattendu, une erreur doit être remontée par la fonction `scan` :

### Exemples :

Le caractère est invalide :
```
 _ 
 | 
 | 
   
```

La grille de 3 x 4 n'est pas respectée :
```
 _
   
```


# Implémentation

La signature de la fonction `scan` est initiée dans le fichier [./ocr/lib.py](./ocr/lib.py).

Des tests d'intégration sont là pour valider l'implémentation de la librairie. Ils sont également là pour guider et faciliter la compréhension du problème.

Les indications `TODO` en commentaire attendent qu'on les remplace !

Libre à vous ensuite de compléter et d'organiser le code et les tests comme bon vous semble.

Enfin, n'hésitez pas à segmenter votre travail en différentes étapes, Git est là pour ça...

Quelques commandes utiles:

 - Exécuter les tests : `python -m unittest discover -v -s tests`
 - Vérifier le style : `python -m pylint ocr/ tests/`


### Bonne chance et soyez vous-même !
